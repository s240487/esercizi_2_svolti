#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;

}
Intersector2D1D::~Intersector2D1D()
{

}

Vector3d Intersector2D1D::IntersectionPoint()
{
    if(IntersectionType()!=2)
    {
        throw runtime_error ("There is no intersection");
    }

    return *lineOriginPointer + intersectionParametricCoordinate*(*lineTangentPointer);
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    double normalDotTangent = (*planeNormalPointer).dot(*lineTangentPointer);
    double normalDotDifferencePlaneLine = *planeTranslationPointer - (*planeNormalPointer).dot(*lineOriginPointer);

    if (-toleranceParallelism <= normalDotTangent && normalDotTangent <= toleranceParallelism)
    {
        // check if n.dot(point_plane - line_origin) = n.dot(point_plane) - n.dot(line_origin) = plane_traslation - n.dot(line_origin) = 0
        // this means the line is inside the plane
        if (-toleranceParallelism <= normalDotDifferencePlaneLine && normalDotDifferencePlaneLine <= toleranceParallelism)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoInteresection;
            return false;
        }
    }

    intersectionType = PointIntersection;
    double s = normalDotDifferencePlaneLine / normalDotTangent;
    intersectionParametricCoordinate = s;
    return true;
}
