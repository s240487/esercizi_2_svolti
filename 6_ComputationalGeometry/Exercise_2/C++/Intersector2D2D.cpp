#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-5;

    matrixNormalVector.setZero();
    rightHandSide.setZero();
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    tangentLine = (matrixNormalVector.row(0)).cross(matrixNormalVector.row(1));
       // check if N_1 is parallel to N_2
       if (tangentLine.squaredNorm() <= toleranceParallelism * toleranceParallelism)
       {
           // check if are coincident, so if plane traslation have the same ratio of normal vectors
           double ratio;
           if (matrixNormalVector(0, 0) >= 1.0E-16)
               ratio = matrixNormalVector(0, 0) / matrixNormalVector(1, 0);
           else if (matrixNormalVector(0, 1) >= 1.0E-16)
               ratio = matrixNormalVector(0, 1) / matrixNormalVector(1, 1);
           else
               ratio = matrixNormalVector(0, 2) / matrixNormalVector(1, 2);

           if (abs(rightHandSide(0) - rightHandSide(1) * ratio) <= 1.0E-16)
           {
               intersectionType = Coplanar;
               return false;
           }
           else
           {
               intersectionType = NoInteresection;
               return false;
           }
       }

       pointLine = matrixNormalVector.colPivHouseholderQr().solve(rightHandSide);
       intersectionType = LineIntersection;
       return true;
}
