#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    ax = x;
    ay = y;
}

Point &Point::operator = (const Point &point)
{
    ax = point.ax;
    ay = point.ay;

    return *this;
}


Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    centro = center;
    sa = a;
    sb = b;
}

double Ellipse::Area() const
{
    return M_PI*sa*sb;
}

Circle::Circle(const Point &center, const int &radius)
{
    centro = center;
    sa = radius;
    sb = radius;
}

double Circle::Area() const
{
    return M_PI*sa*sb;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    v1 = p1;
    v2 = p2;
    v3 = p3;
    a = pow((v1.ax-v2.ax)*(v1.ax-v2.ax) + (v1.ay-v2.ay)*(v1.ay-v2.ay), 0.5);
    b = pow((v2.ax-v3.ax)*(v2.ax-v3.ax) + (v2.ay-v3.ay)*(v2.ay-v3.ay), 0.5);
    c = pow((v3.ax-v1.ax)*(v3.ax-v1.ax) + (v3.ay-v1.ay)*(v3.ay-v1.ay), 0.5);
}

double Triangle::Area() const
{
    double p = (a+b+c)/2;
    return pow(p*(p-a)*(p-b)*(p-c),0.5);
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    v1 = p1;
    a = edge;
    b = edge;
    c = edge;
}

double TriangleEquilateral::Area() const
{
    double p = (a+b+c)/2;
    return pow(p*(p-a)*(p-b)*(p-c), 0.5);
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    v1=p1;
    v2=p2;
    v3=p3;
    v4=p4;
    a = pow((v1.ax-v2.ax)*(v1.ax-v2.ax) + (v1.ay-v2.ay)*(v1.ay-v2.ay), 0.5);
    b = pow((v2.ax-v3.ax)*(v2.ax-v3.ax) + (v2.ay-v3.ay)*(v2.ay-v3.ay), 0.5);
    c = pow((v3.ax-v4.ax)*(v3.ax-v4.ax) + (v3.ay-v4.ay)*(v3.ay-v4.ay), 0.5);
    d = pow((v4.ax-v1.ax)*(v4.ax-v1.ax) + (v4.ay-v1.ay)*(v4.ay-v1.ay), 0.5);
    e = pow((v2.ax-v4.ax)*(v2.ax-v4.ax) + (v2.ay-v4.ay)*(v2.ay-v4.ay), 0.5);
}

double Quadrilateral::Area() const
{
    double p1 = (a+d+e)/2;
    double p2 = (b+c+e)/2;
    return pow(p1*(p1-a)*(p1-d)*(p1-e),0.5) + pow(p2*(p2-b)*(p2-c)*(p2-e),0.5);
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    v1 = p1;
    v2 = p2;
    v4 = p4;
    a = pow((v1.ax-v2.ax)*(v1.ax-v2.ax) + (v1.ay-v2.ay)*(v1.ay-v2.ay), 0.5);
    d = pow((v4.ax-v1.ax)*(v4.ax-v1.ax) + (v4.ay-v1.ay)*(v4.ay-v1.ay), 0.5);
    e = pow((v2.ax-v4.ax)*(v2.ax-v4.ax) + (v2.ay-v4.ay)*(v2.ay-v4.ay), 0.5);
}

double Parallelogram::Area() const
{
    double p = (a+d+e)/2;
    return 2*pow(p*(p-a)*(p-d)*(p-e), 0.5);
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    v1 = p1;
    a = base;
    d = height;
    e = pow(a*a + d*d,0.5);
}

double Rectangle::Area() const
{
    double p = (a+d+e)/2;
    return 2*pow(p*(p-a)*(p-d)*(p-e), 0.5);
}

Square::Square(const Point &p1, const int &edge)
{
    v1 = p1;
    a = edge;
    d = a;
    e = pow(2,0.5)*edge;
}

double Square::Area() const
{
    double p = (a+d+e)/2;
    return 2*pow(p*(p-a)*(p-d)*(p-e),0.5);
}
}
