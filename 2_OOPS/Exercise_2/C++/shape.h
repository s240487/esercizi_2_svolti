#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double ax,ay;
      Point(const double& x,
            const double& y);
      Point(const Point& point) //costruttore copia
      {
          ax = point.ax;
          ay = point.ay;
      }
      Point& operator = (const Point& point); //operatore di assegnazione
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point centro = Point (0.0,0.0);
      int sa, sb;

    public:
      Ellipse(const Point& center= Point(0.0,0.0),
              const int& a=0,
              const int& b=0);

      double Area() const;
  };

  class Circle : virtual public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
    protected:
      Point v1=Point(0.0,0.0), v2=Point(0.0,0.0), v3=Point(0.0,0.0);
      double a,b,c;
    public:
      Triangle(const Point& p1 = Point(0.0,0.0),
               const Point& p2 = Point(0.0,0.0),
               const Point& p3 = Point(0.0,0.0));

      double Area() const;
  };


  class TriangleEquilateral : virtual public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point v1=Point(0.0,0.0), v2=Point(0.0,0.0), v3=Point(0.0,0.0), v4=Point(0.0,0.0);
      double a,b,c,d,e;

    public:
      Quadrilateral(const Point& p1 = Point(0.0,0.0),
                    const Point& p2 = Point(0.0,0.0),
                    const Point& p3 = Point(0.0,0.0),
                    const Point& p4 = Point(0.0,0.0));

      double Area() const;
  };


  class Parallelogram : virtual public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1 = Point(0.0,0.0),
                    const Point& p2 = Point(0.0,0.0),
                    const Point& p4 = Point(0.0,0.0));

      double Area() const;
  };

  class Rectangle : virtual public Parallelogram
  {
    public:
      Rectangle(const Point& p1 = Point(0.0,0.0),
                const int& base = 0,
                const int& height = 0);

      double Area() const;
  };

  class Square: virtual public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
