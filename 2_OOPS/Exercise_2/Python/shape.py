import math


class Point:
    def __init__(self, x: float, y: float):
        self.ax = x
        self.ay = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._centro = center
        self._sa = a
        self._sb = b

    def area(self) -> float:
        return math.pi * self._sa * self._sb


class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        super().__init__()
        self._centro = center
        self._raggio = radius

    def area(self):
        return math.pi * self._raggio * self._raggio


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._v1 = p1
        self._v2 = p2
        self._v3 = p3

    def area(self) -> float:
        _a: float = pow((self._v1.ax - self._v2.ax) * (self._v1.ax - self._v2.ax) +
                        (self._v1.ay - self._v2.ay) * (self._v1.ay - self._v2.ay), 0.5)
        _b: float = pow((self._v2.ax - self._v3.ax) * (self._v2.ax - self._v3.ax) +
                        (self._v2.ay - self._v3.ay) * (self._v2.ay - self._v3.ay), 0.5)
        _c: float = pow((self._v3.ax - self._v1.ax) * (self._v3.ax - self._v1.ax) +
                        (self._v3.ay - self._v1.ay) * (self._v3.ay - self._v1.ay), 0.5)
        p = (_a + _b + _c) / 2
        return pow(p * (p - _a) * (p - _b) * (p - _c), 0.5)


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        super().__init__()
        self._v1 = p1
        self._a = edge
        self._b = edge
        self._c = edge

    def area(self) -> float:
        p: float = (self._a + self._b + self._c) / 2
        return pow(p * (p - self._a) * (p - self._b) * (p - self._c), 0.5)


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._v1 = p1
        self._v2 = p2
        self._v3 = p3
        self._v4 = p4

    def area(self) -> float:
        _a: float = pow((self._v1.ax - self._v2.ax) * (self._v1.ax - self._v2.ax) +
                        (self._v1.ay - self._v2.ay) * (self._v1.ay - self._v2.ay), 0.5)
        _b: float = pow((self._v2.ax - self._v3.ax) * (self._v2.ax - self._v3.ax) +
                        (self._v2.ay - self._v3.ay) * (self._v2.ay - self._v3.ay), 0.5)
        _c: float = pow((self._v3.ax - self._v4.ax) * (self._v3.ax - self._v4.ax) +
                        (self._v3.ay - self._v4.ay) * (self._v3.ay - self._v4.ay), 0.5)
        _d: float = pow((self._v4.ax - self._v1.ax) * (self._v4.ax - self._v1.ax) +
                        (self._v4.ay - self._v1.ay) * (self._v4.ay - self._v1.ay), 0.5)
        _e: float = pow((self._v2.ax - self._v4.ax) * (self._v2.ax - self._v4.ax) +
                        (self._v2.ay - self._v4.ay) * (self._v2.ay - self._v4.ay), 0.5)
        p1: float = (_a + _d + _e) / 2
        p2: float = (_b + _c + _e) / 2
        return pow(p1 * (p1 - _a) * (p1 - _d) * (p1 - _e), 0.5) + pow(p2 * (p2 - _b) * (p2 - _c) * (p2 - _e), 0.5)


class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__()
        self._v1 = p1
        self._v2 = p2
        self._v4 = p4

    def area(self) -> float:
        a = pow((self._v1.ax - self._v2.ax) * (self._v1.ax - self._v2.ax) +
                (self._v1.ay - self._v2.ay) * (self._v1.ay - self._v2.ay), 0.5)
        d = pow((self._v4.ax - self._v1.ax) * (self._v4.ax - self._v1.ax) +
                (self._v4.ay - self._v1.ay) * (self._v4.ay - self._v1.ay), 0.5)
        e = pow((self._v2.ax - self._v4.ax) * (self._v2.ax - self._v4.ax) +
                (self._v2.ay - self._v4.ay) * (self._v2.ay - self._v4.ay), 0.5)
        p: float = (a + d + e) / 2
        return 2 * pow(p * (p - a) * (p - d) * (p - e), 0.5)


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__()
        self._v1 = p1
        self._a = base
        self._d = height
        self._e = pow(self._a * self._a + self._d * self._d, 0.5)

    def area(self) -> float:
        p: float = (self._a + self._d + self._e) / 2
        return 2 * pow(p * (p - self._a) * (p - self._d) * (p - self._e), 0.5)


class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        super().__init__()
        self._v1 = p1
        self._a = edge
        self._d = self._a
        self._e = pow(2, 0.5) * edge

    def area(self) -> float:
        p: float = (self._a + self._d + self._e) / 2
        return 2 * pow(p * (p - self._a) * (p - self._d) * (p - self._e), 0.5)
