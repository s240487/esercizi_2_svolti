#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

void Ellipse::AddVertex(const Point &point)
{
    _center = point;
    *this = Ellipse(_center,_a, _b);
}

double Ellipse::Perimeter() const
{
    return 2*M_PI*pow((_a*_a+_b*_b)/2,0.5);
}

Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
{
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
}

void Triangle::AddVertex(const Point &point)
{
    points.push_back(point);

    if(points.size() == 3)
    {
        *this = Triangle(points[0],points[1],points[2]);
    }
}

double Triangle::Perimeter() const
{
    double perimeter = 0;


    for(int i = 0; i < 3; i++)
    {
        perimeter += (double)(points[(i+1)%3]-points[i]).ComputeNorm2();

    }

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    points.push_back(p1);
    points.push_back(Point(p1.X,p1.Y+edge));
    points.push_back(Point(p1.X+0.5*edge*pow(3,0.5),p1.Y+0.5*edge));
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      Quadrilateral();

      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  void Quadrilateral::AddVertex(const Point &p)
  {
      points.push_back(p);

      if(points.size() == 4)
      {
          *this = Quadrilateral(points[0],points[1],points[2],points[3]);
      }
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;

    for(int i = 0; i < 4; i++)
    {
        perimeter += (double)(points[(i+1)%4]-points[i]).ComputeNorm2();
    }

    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      Rectangle();

      points.push_back(p1);
      points.push_back(Point(p1.X+base,p1.Y));
      points.push_back(Point(p1.X+base,p1.Y+height));
      points.push_back(Point(p1.X,p1.Y+height));      
  }

  Square::Square(const Point &p1, const double &edge)
  {
      Square();

      points.push_back(p1);
      points.push_back(Point(p1.X+edge,p1.Y));
      points.push_back(Point(p1.X+edge,p1.Y+edge));
      points.push_back(Point(p1.X,p1.Y+edge));      
  }




  Point Point::operator+(const Point& point) const
  {
      Point punto(X,Y);

      punto.X = X + point.X;
      punto.Y = Y + point.Y;

      return punto;
  }

  Point Point::operator-(const Point& point) const
  {
      Point punto(X,Y);

      punto.X = X - point.X;
      punto.Y = Y - point.Y;

      return punto;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;

      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;

      return *this;
  }

  Point &Point::operator=(const Point &point)
  {
      X = point.X;
      Y = point.Y;

      return *this;
  }



}
