class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self):  #__init__(self, name: str):
        self.Name = ""
        self.__ingredients = [] #vector of Ingredient

    def addIngredient(self, ingredient: Ingredient):
        self.__ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.__ingredients)

    def computePrice(self) -> int:
        pizzaPrice: int = 0
        for ingredient in self.__ingredients:
            pizzaPrice += ingredient.Price
        return pizzaPrice


class Order:
    def __init__(self):
        self.__pizzas = [] #vector of Pizza
        self.numOrder = 0

    def getPizza(self, position: object) -> object:
        numPizzas = self.numPizzas()
        if position < 1 or position > numPizzas:
            raise Exception("Position passed is wrong")
        else:
            return self.__pizzas[position -1]


    def initializeOrder(self, numPizzas: int):
        self.__pizzas = [ ]*numPizzas

    def addPizza(self, pizza: Pizza):
        self.__pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.__pizzas)

    def computeTotal(self) -> int:
        total = 0
        for pizza in self.__pizzas:
            total +=  pizza.computePrice()
        return total


class Pizzeria:
    def __init__(self):
        self.__orders = []  #list of orders
        self.__mapNameToIngredient = {} # dizionario string, Ingredient
        self.__mapNameToPizza = {} # dizionario string, Pizza

    def addIngredient(self, name: str, description: str, price: int):
        if self.__mapNameToIngredient.get(name) is None:
            ingredient = Ingredient(name, price, description)
            self.__mapNameToIngredient[name] = ingredient
        else:
            raise Exception("Ingredient already inserted")


    def findIngredient(self, name: str) -> Ingredient:
        ingredient = self.__mapNameToIngredient.get(name)
        if ingredient is not None:
            return ingredient
        else:
            raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        pizza = self.__mapNameToPizza.get(name)
        if pizza is not None:   #se  c'è ,errore
            raise Exception("Pizza already inserted")
        else:
            newPizza = Pizza()
            newPizza.Name = name
            for ingredName in ingredients:
                foundIngredient = self.findIngredient(ingredName)
                 # non fare così: foundIngredient = self.findIngredient(ingredients[i]) dà errore  'set' object is not subscriptable
                 # si vede nel test righa 89 che il secondo argomento è un set (usa delle {}). Se fosse stata una lista non ci sarebbero stai problemi
                newPizza.addIngredient(foundIngredient)
            self.__mapNameToPizza[name] = newPizza

    def findPizza(self, name: str) -> Pizza:
        pizza = self.__mapNameToPizza.get(name)
        if(pizza is not None):
            return pizza
        else:
            raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")
        else:
            order = Order()
            order.initializeOrder(len(pizzas))
            i = 0
            for pizzaName in pizzas:
                order.addPizza( self.findPizza(pizzaName))
                i += 1
            order.numOrder = 1000 + len(self.__orders)
            self.__orders.append(order)
            return order.numOrder

    def findOrder(self, numOrder: int) -> Order:
        numOrders = len(self.__orders)
        orderPosition = numOrder - 1000
        if orderPosition < 0 or orderPosition >= numOrders:
            raise Exception("Order not found")
        else:
            return self.__orders[orderPosition]

    def getReceipt(self, numOrder: int) -> str:
        order = self.findOrder(numOrder)
        total = order.computeTotal()
        receipt: str =""
        for i in range (1, order.numPizzas()+1):
            pizza = order.getPizza(i)
            receipt = receipt + "- " + pizza.Name + ", " +str(pizza.computePrice()) + " euro" + "\n"
        receipt = receipt + "  TOTAL: " + str(total) + " euro" + "\n"
        return receipt


    def listIngredients(self) -> str:
        s = ''
        for name in sorted(self.__mapNameToIngredient):    #Ci servono gli elementi in ordine alfabetico
            s = s + self.__mapNameToIngredient[name].Name + " - '" + self.__mapNameToIngredient[name].Description + "': " + str(self.__mapNameToIngredient[name].Price) + " euro" + "\n"
        return s

    def menu(self) -> str:
        s: str = ""
        for name in self.__mapNameToPizza:
            s = s + name + " (" + str(self.__mapNameToPizza[name].numIngredients()) + " ingredients): " + str(self.__mapNameToPizza[name].computePrice())+ " euro" + "\n"
        print(s)
        return s
