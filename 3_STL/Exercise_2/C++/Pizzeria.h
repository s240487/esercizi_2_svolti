#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
    private:
      vector<Ingredient> Ingredienti;

  public:
      string Name;

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const ;
      void Reset();

  };

  class Order {
    private:
      vector<Pizza> Ordine;
    public:
      int numOrdine;
      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const;
  };

  class Pizzeria {
      vector<Ingredient> elencoIngredienti;
      vector<Pizza> elencoPizze;
      vector<Order> elencoOrdini;
    public:
      Ingredient nuovoIngrediente;
      Pizza nuovaPizza;
      Order nuovoOrdine;
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
