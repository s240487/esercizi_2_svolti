#include "Pizzeria.h"

namespace PizzeriaLibrary {

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    Ingredienti.push_back(ingredient);//mette in coda nel vettore
}

int Pizza::NumIngredients() const
{
    return Ingredienti.size();//la taglia ossia la dim del vettore
}

int Pizza::ComputePrice() const
{
    int prezzoPizza=0; //Compute price deve ritornare un int (e non unsigned int)
    for(int i=0; i<NumIngredients(); i++)
    {
        prezzoPizza=prezzoPizza+Ingredienti[i].Price;
    }
    return prezzoPizza;
}

void Pizza::Reset()
{
    Ingredienti.clear();
}

void Order::InitializeOrder(int numPizzas)
{
    Ordine.reserve(numPizzas);//riserva tot posti in memoria
}

void Order::AddPizza(const Pizza &pizza)
{
    Ordine.push_back(pizza); //metti in coda la pizza che passo per referenza
}

const Pizza &Order::GetPizza(const int &position) const
{
    if(position>(int)Ordine.size()||position<1) //se è fuori dal vettore lancia errore
    {
        throw runtime_error("Position passed is wrong");
    }
    else
    {
        return Ordine[position-1];//il vettore non ha il primo elemento che corrisponderebbe a 0, ma si parte da 1
    }
}

int Order::NumPizzas() const
{
    return Ordine.size(); // la taglia delvettore di pizze mi da il numero di pizze
}

int Order::ComputeTotal() const
{
    int somma=0;
    for(int i=0; i<NumPizzas(); i++)
    {
        somma+=Ordine[i].ComputePrice();
    }
    return somma;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    nuovoIngrediente.Name = name;
    nuovoIngrediente.Description = description;
    nuovoIngrediente.Price = price;

    unsigned int dimIngredienti=elencoIngredienti.size();
    for(unsigned int i=0;i<dimIngredienti ; i++)
    {
        if(nuovoIngrediente.Name == elencoIngredienti[i].Name)
        {
            throw runtime_error("Ingredient already inserted");
        }
    }
    elencoIngredienti.push_back(nuovoIngrediente);

    Ingredient tmp;

        for(unsigned int i=0; i < dimIngredienti; i++)
        {
            if(elencoIngredienti[i].Name>elencoIngredienti[i+1].Name)
            {
                tmp=elencoIngredienti[i+1];
                elencoIngredienti[i+1]=elencoIngredienti[i];
                elencoIngredienti[i]=tmp;
            }
        }
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    unsigned int dimIngredienti = elencoIngredienti.size();
    for(unsigned int i=0; i<dimIngredienti; i++)
    {
        if(name==elencoIngredienti[i].Name)
            return elencoIngredienti[i];
    }
    throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    nuovaPizza.Reset();
    nuovaPizza.Name = name;
    unsigned int dimPizze = elencoPizze.size();
    for(unsigned int i=0; i < dimPizze ; i++)
    {
        if(nuovaPizza.Name == elencoPizze[i].Name)
        {
          throw runtime_error("Pizza already inserted");
        }
    }
    unsigned int dimIngredienti= elencoIngredienti.size();
    unsigned int ingredientiMolt= ingredients.size();
    for(unsigned int i=0; i<ingredientiMolt; i++)
    {
        for(unsigned int j=0; j<dimIngredienti; j++)
        {
            if(ingredients[i]==elencoIngredienti[j].Name)
                nuovaPizza.AddIngredient(elencoIngredienti[j]);
        }
    }
    elencoPizze.push_back(nuovaPizza);
    Pizza tmp;
        for(unsigned int i=0; i < dimPizze; i++)
        {
            if(elencoPizze[i].ComputePrice()>elencoPizze[i+1].ComputePrice())
            {
                tmp=elencoPizze[i+1];
                elencoPizze[i+1]=elencoPizze[i];
                elencoPizze[i]=tmp;
            }
        }
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned int dimPizze = elencoPizze.size();
    for(unsigned int i=0; i<dimPizze; i++)
    {
        if(name==elencoPizze[i].Name)
            return elencoPizze[i];
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    if(pizzas.empty())
        throw runtime_error("Empty order");
    nuovoOrdine.InitializeOrder(pizzas.size());
    unsigned int pizzasDim = pizzas.size();

    for(unsigned int i=0; i<pizzasDim; i++)
    {
        unsigned int dimPizze = elencoPizze.size();
        for(unsigned int j=0; j< dimPizze; j++)
        {
            if(pizzas[i]==elencoPizze[j].Name)
            {
                nuovaPizza=elencoPizze[j];
                nuovoOrdine.AddPizza(nuovaPizza);
            }
        }
    }
    nuovoOrdine.numOrdine= 1000+elencoOrdini.size();
    elencoOrdini.push_back(nuovoOrdine);
    return  nuovoOrdine.numOrdine;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    unsigned int dimOrdini = elencoOrdini.size();
    for(unsigned int i = 0; i< dimOrdini; i++)
    {
       if(numOrder==elencoOrdini[i].numOrdine)
           return elencoOrdini[i];
    }
    throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string scontrino = "";
    unsigned int dimOrdini=elencoOrdini.size();

    for(unsigned i=0; i<dimOrdini; i++)
    {
        if(numOrder==elencoOrdini[i].numOrdine)
        {
            for(int k=1; k<=elencoOrdini[i].NumPizzas(); k++)
            {
                scontrino += "- " + elencoOrdini[i].GetPizza(k).Name + ", " +
                        to_string(elencoOrdini[i].GetPizza(k).ComputePrice())+ " euro" + "\n";
            }
            scontrino += "  TOTAL: " + to_string(elencoOrdini[i].ComputeTotal())+ " euro" + "\n";
            return scontrino;
        }
    }
    throw runtime_error("Order not found");
}

string Pizzeria::ListIngredients() const
{
    string ElencoIngredienti;

        unsigned int dimIngredienti = elencoIngredienti.size();



        for(unsigned int i = 0 ; i < dimIngredienti; i++)

        {

           ElencoIngredienti += elencoIngredienti[i].Name + " - '" + elencoIngredienti[i].Description
                   + "': " + to_string(elencoIngredienti[i].Price) + " euro" + "\n";

        }

        return ElencoIngredienti;

}

string Pizzeria::Menu() const
{
    string ElencoPizze;

        unsigned int dimPizze = elencoPizze.size();



        for(unsigned int i = 0 ; i < dimPizze; i++)

        {

           ElencoPizze += elencoPizze[i].Name + " (" + to_string(elencoPizze[i].NumIngredients())
                   + " ingredients): " + to_string(elencoPizze[i].ComputePrice()) + " euro" + "\n";

        }

        return ElencoPizze;
}

}
