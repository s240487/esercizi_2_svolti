# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {


    void BusStation::Load() //legge da file caricando tutta l'informazione sui Bus
    {

        /// Reset station
        _numberBus = 0;
        _buses.clear();

        /// Open File
        ifstream file;
        file.open(_busFilePath);

        if(file.fail())
            throw runtime_error("Something goes wrong");

        /// Load buses
        try {
            string line;
            getline(file, line); //skip comment line "# Number of buses"
            getline(file, line);
            istringstream convertN;
            convertN.str(line);
            convertN >> _numberBus;

            getline(file, line); //skip comment line "# Buses"
            getline(file, line); //skip comment line # Line FuelCost(EUR/km)

            _buses.resize(_numberBus);

            /// Fill Bus Info
            for (int b = 0; b < _numberBus; b++)
            {
                 getline(file, line);
                 istringstream converter;
                 converter.str(line);
                 converter >> _buses[b].Id >>_buses[b].FuelCost;
            }
            /// Close File
            file.close();

        }catch (exception){
            _numberBus = 0;
            _buses.clear();
            throw runtime_error("Something goes wrong");
        }
    }

    const Bus& BusStation::GetBus(const int& idBus) const
    {
        if (idBus < 1 || idBus > _numberBus )
            throw runtime_error("Bus " + to_string(idBus) + " does not exists");
        else
            return _buses[idBus-1];
    }


    void MapData::Reset()
      {
        _numberRoutes = 0;
        _numberStreets = 0;
        _numberBusStops = 0;
        _busStops.clear();
        _streets.clear();
        _routes.clear();
        _streetsFrom.clear();
        _streetsTo.clear();
        _routeStreets.clear();
      }



    void MapData::Load() //legge da file caricando tutta l'informazione sulla mappa
    {
        Reset();
        ifstream file;
        file.open(_mapFilePath);
        if (file.fail())
            throw runtime_error("Something goes wrong");

        try
        {
            string line;
            getline(file, line);  //skip comment line "# Number of busStop"
            getline(file, line);
            istringstream convertNumBusStops;
            convertNumBusStops.str(line);
            convertNumBusStops >> _numberBusStops;

            getline(file,line);  //skip comment line "# Id Name Lat Lon"
            _busStops.resize(_numberBusStops);
            for (int bs = 0; bs < _numberBusStops; bs++)
            {
               getline(file, line);
               istringstream converter;
               converter.str(line);
               converter >> _busStops[bs].Id >> _busStops[bs].Name >>_busStops[bs].Latitude >>  _busStops[bs].Longitude;
            }

            getline(file, line);  //skip comment line "# Number of Streets"
            getline(file, line);
            istringstream converterNumberStreets;
            converterNumberStreets.str(line);
            converterNumberStreets >> _numberStreets;

            _streets.resize(_numberStreets);
            _streetsFrom.resize(_numberStreets);
            _streetsTo.resize(_numberStreets);
            getline(file, line);  //skip comment line "# Id From To TravelTime(s)"
            for (int s = 0; s < _numberStreets; s++)
            {
                getline(file, line);
                istringstream converter;
                converter.str(line);
                converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime;
            }

            getline(file,line);  //skip comment line "# Number of Routes"
            getline(file,line);
            istringstream converterNumbRoutes;
            converterNumbRoutes.str(line);
            converterNumbRoutes >> _numberRoutes;

            getline(file,line);  //skip comment line "# Id NumberStreets StreetIds"
            _routes.resize(_numberRoutes);
            _routeStreets.resize(_numberRoutes);
            for (int r = 0; r < _numberRoutes; r++)
            {
                getline(file, line);
                istringstream converter;
                converter.str(line);
                converter >> _routes[r].Id >> _routes[r].NumberStreets;
                _routeStreets[r].resize(_routes[r].NumberStreets);
                for(int s = 0; s < _routes[r].NumberStreets; s++ )
                {
                    converter >>_routeStreets[r][s];
                }
            }
            file.close();
         }catch (exception){
         Reset();
         throw runtime_error("Something goes wrong");
        }
    }

    const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
    {
        if (idRoute > _numberRoutes)
            throw runtime_error("Route " + to_string(idRoute) + " does not exists");

          const Route& route = _routes[idRoute - 1];

          if (streetPosition >= route.NumberStreets)
            throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

          int idStreet = _routeStreets[idRoute - 1][streetPosition];

          return _streets[idStreet - 1];
    }

    const Route& MapData::GetRoute(const int& idRoute) const
    {
      if (idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

      return _routes[idRoute - 1];
    }

    const Street& MapData::GetStreet(const int& idStreet) const
    {
      if (idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

      return _streets[idStreet - 1];
    }

    const BusStop& MapData::GetStreetFrom(const int& idStreet) const
    {
      if (idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

      int idFrom = _streetsFrom[idStreet - 1];

      return _busStops[idFrom - 1];
    }

    const BusStop& MapData::GetStreetTo(const int& idStreet) const
    {
      if (idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

      int idTo = _streetsTo[idStreet - 1];

      return _busStops[idTo - 1];
    }

    const BusStop& MapData::GetBusStop(const int& idBusStop) const
    {
      if (idBusStop > _numberBusStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

      return _busStops[idBusStop - 1];
    }

    //initilization of static member
    int RoutePlanner::BusAverageSpeed = 50;   //in km/h

    int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
    {
        const Route& route = _mapData.GetRoute(idRoute); //GetRoute ritorna un const
        int totalTravelTime = 0;
        for(int s = 0; s < route.NumberStreets; s++)
        {
            Street street = _mapData.GetRouteStreet(idRoute, s);
            totalTravelTime += street.TravelTime;
        }
        return totalTravelTime;
    }

    int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
    {

        const Bus& bus =_busStation.GetBus(idBus);
        double costEuroAlKm  = bus.FuelCost;
        int time_s = ComputeRouteTravelTime(idRoute);
        double velocitykms = (double)RoutePlanner::BusAverageSpeed / 3600;
        int totalTravelCost = time_s * costEuroAlKm * velocitykms;

        return totalTravelCost;
    }



    string MapViewer::ViewRoute(const int &idRoute) const
    {
        const Route& route = _mapData.GetRoute(idRoute);
        int s = 0;
        ostringstream routeView;
        routeView << to_string(idRoute) <<": ";
        for (; s < route.NumberStreets - 1; s++)
        {
            int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
            string from = _mapData.GetStreetFrom(idStreet).Name;
            routeView << from<< " -> ";
        }

        int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
        string from = _mapData.GetStreetFrom(idStreet).Name;
        string to = _mapData.GetStreetTo(idStreet).Name;
        routeView << from<< " -> "<< to;

        return routeView.str();
    }

  string MapViewer::ViewStreet(const int& idStreet) const
  {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
  }

  string MapViewer::ViewBusStop(const int& idBusStop) const
  {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
  }


}
