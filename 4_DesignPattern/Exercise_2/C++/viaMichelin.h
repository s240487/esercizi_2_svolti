#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
      string _busFilePath;
      int _numberBus;
      vector<Bus> _buses;

    public:
      BusStation(const string& busFilePath) { _busFilePath = busFilePath;}

      void Load();
      int NumberBuses() const  { return _numberBus; }
      const Bus& GetBus(const int& idBus) const ;
  };

  class MapData : public IMapData {
      string _mapFilePath;
      int _numberBusStops;
      int _numberStreets;
      int _numberRoutes;
      vector<BusStop> _busStops;
      vector<Street> _streets;
      vector<Route> _routes;
      vector<int> _streetsFrom; //strade di partenza di ogni fermata
      vector<int> _streetsTo;   //strade di partenza 'arrivo di ogni fermata
      vector<vector<int>> _routeStreets; //ogni elemento è un vettor contenenti le strade di una ruta

    public:
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath; }
      void Load() ;
      int NumberRoutes() const { return _numberRoutes; }
      int NumberStreets() const { return _numberStreets; }
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
      void Reset();
  };

  class RoutePlanner : public IRoutePlanner {
    const IMapData& _mapData;
    const IBusStation& _busStation;

    public:
      static int BusAverageSpeed;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) :_mapData(mapData), _busStation(busStation){ }
       //le referenze vengono riempite prima che il costruttore venga chiamato
      int ComputeRouteTravelTime(const int& idRoute) const ;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
      const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData): _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
